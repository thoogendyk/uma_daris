/**
 * @file
 * Behaviors for the Carousel.
 */
!function (Drupal, $) {
  'use strict';
  /**
   * Setup and attach the Carousel behaviors.
   *
   * @type {Drupal~behavior}
   */

  Drupal.behaviors.carousel = {
    attach: function attach() {
      $('.carousel__slick').not('.slick-initialized').slick({
        dots: true
      });
    }
  };
}(Drupal, jQuery);
//# sourceMappingURL=carousel.js.map
